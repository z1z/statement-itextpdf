package kz.edu.astanait;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.draw.SolidLine;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class ITextPDF {
    private static final String PATH = "D:" + File.separator + "Заявление.pdf";
    private static final String FONT_PATH = "src" + File.separator + "main" + File.separator + "resources"
            + File.separator + "times.ttf";
    private static final String AITU_LOGO_PATH = "src" + File.separator + "main" + File.separator + "resources"
            + File.separator + "Astana IT University.png";

    private Style basicStyle = new Style()
            .setMarginTop(0F)
            .setMarginBottom(0F);

    private Style underParagraphStyle = new Style(basicStyle)
            .setItalic()
            .setFontSize(8);

    public void createPDF() {
        try {
            PdfWriter pdfWriter = new PdfWriter(PATH);

            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            pdfDocument.addNewPage();

            Document document = new Document(pdfDocument, PageSize.A4);
            document.setMargins(40F, 42.5F, 40F, 60F);

            PdfFont font = PdfFontFactory.createFont(FONT_PATH, PdfEncodings.IDENTITY_H);
            document.setFont(font);
            document.setFontSize(12);

            createHeader(document);
            createFirstBlock(document);
            createSecondBlock(document);
            createThirdBlock(document);
            createFourthBlock(document);
            createFooter(document);

            pdfDocument.addNewPage();

            createHeaderKaz(document);
            createFirstBlockKaz(document);
            createSecondBlockKaz(document);
            createThirdBlockKaz(document);
            createFourthBlockKaz(document);
            createFooterKaz(document);

            document.close();
            System.out.println("Done!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createHeader(Document document) {
        UnitValue[] columnWidth = {
                UnitValue.createPercentValue(40),
                UnitValue.createPercentValue(20),
                UnitValue.createPercentValue(40)
        };
        Table table = new Table(columnWidth)
                .setWidth(UnitValue.createPercentValue(100));

        Image aituLogo = null;
        try {
            ImageData aituLogoData = ImageDataFactory.create(AITU_LOGO_PATH);
            aituLogo = new Image(aituLogoData).setHeight(60f);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        table.addCell(new Cell().add(aituLogo).setBorder(null));

        table.addCell(new Cell().setBorder(null));

        table.addCell(
                new Cell().add(new Paragraph("Ректору \n" +
                        "Astana IT University \n" +
                        "Ахмед-Заки Д.Ж. \n")
                        .setBold())
                        .setVerticalAlignment(VerticalAlignment.MIDDLE)
                        .setBorder(null)
        );

        document.add(table);
    }

    private void createFirstBlock(Document document) {
        addParagraphWithUnderscoresAtTheEnd(document, "От ");

        document.add(getUnderParagraph("(фамилия, имя, отчество)"));

        addParagraphWithUnderscoresAtTheEnd(document, "проживающего(ей):");
        document.add(getUnderParagraph("(область, город, улица, дом, квартира)"));

        addParagraphWithUnderscoresAtTheEnd(document, "Удостоверение/паспорт №______ выдано (кем) ");
        addParagraphWithUnderscoresAtTheEnd(document, "дата выдачи ________________ Гражданство ");
        addParagraphWithUnderscoresAtTheEnd(document, "Национальность_______________ Дата рождения ");
        addParagraphWithUnderscoresAtTheEnd(document, "ИИН ___________________ Телефон (дом.) ");
        addParagraphWithUnderscoresAtTheEnd(document, "Телефон (моб.)_______________________ e-mail ");
        addParagraphWithUnderscoresAtTheEnd(document, "окончившего(ей) в _____г.  ");
        document.add(getParagraphWithUnderStyle("(наименование учебного заведения, город)").setTextAlignment(TextAlignment.RIGHT).setMarginRight(80f));
    }

    private void createSecondBlock(Document document) {
        document.add(new Paragraph("ЗАЯВЛЕНИЕ")
                .setBold()
                .setTextAlignment(TextAlignment.CENTER)
                .setFontSize(12));

        document.add(getParagraphBasicStyle("Прошу Вас принять меня на обучение в Astana IT University на программу Бакалавриата по: "));
        addParagraphWithUnderscoresAtTheEnd(document, "Группе образовательных программ ");
        addParagraphWithUnderscoresAtTheEnd(document, "Образовательной программе ");
        document.add(getParagraphBasicStyle("Являюсь обладателем сертификата ЕНТ № ___________ набрал(а) _________ баллов."));
    }

    private void createThirdBlock(Document document) {
        document.add(new Paragraph());
        document.add(getParagraphBasicStyle("Сведения о родителях/опекунах (для программ Бакалавриата): ")
                .setBold());
        addParagraphWithUnderscoresAtTheEnd(document, "Ф.И.О. матери/опекуна: ");
        addParagraphWithUnderscoresAtTheEnd(document, "Место работы/должность:");
        addParagraphWithUnderscoresAtTheEnd(document, "тел: _____________________________ e-mail: ");
        addParagraphWithUnderscoresAtTheEnd(document, "Ф.И.О. отца/опекуна:");
        addParagraphWithUnderscoresAtTheEnd(document, "Место работы/должность:");
        addParagraphWithUnderscoresAtTheEnd(document, "тел: _____________________________ e-mail: ");
        document.add(getParagraphBasicStyle("Количество детей в семье: ______"));
    }

    private void createFourthBlock(Document document) {
        document.add(new Paragraph());
        document.add(getParagraphBasicStyle("Дополнительные сведения")
                .setBold()
                .setTextAlignment(TextAlignment.CENTER));

        document.add(
                new Paragraph(new Text("1. Инвалидность ")).addStyle(basicStyle)
                        .add(new Text("(нужное подчеркнуть) ").setItalic().setFontSize(10))
                        .add("имеется/ не имеется ")
        );
        document.add(getParagraphBasicStyle("Группа № __________, категория (иналид детства/ приобретенное) ________________"));
        document.add(getParagraphBasicStyle("Свидетельство № ______________, дата выдачи ______________________"));
        document.add(
                new Paragraph(new Text("2. Сирота ")).addStyle(basicStyle)
                        .add(new Text("(нужное подчеркнуть) ").setItalic().setFontSize(10))
                        .add("полная/ без матери, без отца")
        );
        document.add(getParagraphBasicStyle("3.Особые отличия (\"Алтын белгі\", победитель или призер республиканских и международных научных соревнований школьников и олимпиад по общеобразовательным предметам, конкурсов исполнителей и спортивных соревнований)"));
        addParagraphWithUnderscoresAtTheEnd(document,"\n");
        addParagraphWithUnderscoresAtTheEnd(document,"\n");
        addParagraphWithUnderscoresAtTheEnd(document,"4. Где вы будете проживать на время обучения – у родителей, у родственников, на квартире один/ с друзьями, в общежитии (указать) ");
    }

    private void createFooter(Document document){
        document.add(new Paragraph());
        document.add(getParagraphBasicStyle(" «____»______________ 20____ г.\t\t\t\t\tПодпись абитуриента _____________ "));
    }


    private void createHeaderKaz(Document document) {
        UnitValue[] columnWidth = {
                UnitValue.createPercentValue(40),
                UnitValue.createPercentValue(20),
                UnitValue.createPercentValue(40)
        };
        Table table = new Table(columnWidth)
                .setWidth(UnitValue.createPercentValue(100));

        Image aituLogo = null;
        try {
            ImageData aituLogoData = ImageDataFactory.create(AITU_LOGO_PATH);
            aituLogo = new Image(aituLogoData).setHeight(60f);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        table.addCell(new Cell().add(aituLogo).setBorder(null));

        table.addCell(new Cell().setBorder(null));

        table.addCell(
                new Cell().add(new Paragraph("Astana IT University\n" +
                        "ректоры\n" +
                        "Ахмед-Заки Д.Ж. мырзаға\n")
                        .setBold())
                        .setVerticalAlignment(VerticalAlignment.MIDDLE)
                        .setBorder(null)
        );

        document.add(table);
    }

    private void createFirstBlockKaz(Document document) {
        addParagraphWithUnderscoresAtTheEnd(document, "\n");

        document.add(getUnderParagraph("(тегі, аты, әкесінің аты)"));

        addParagraphWithUnderscoresAtTheStart(document, " мекенжайда тұратын");
        document.add(getUnderParagraph("(облыс, қала, көше, үй, пәтер)"));

        addParagraphWithUnderscoresAtTheEnd(document, "Жеке куәлік/төлқұжат №______ берген мекеме ");
        addParagraphWithUnderscoresAtTheEnd(document, "Берілген уақыты ________________ азаматтығы ");
        addParagraphWithUnderscoresAtTheEnd(document, "Ұлты_______________ туған күні, айы, жылы ");
        addParagraphWithUnderscoresAtTheEnd(document, "ЖСН ___________________ Телефон (үй) ");
        addParagraphWithUnderscoresAtTheEnd(document, "Телефон (моб.)_______________________ e-mail ");
        document.add(getParagraphBasicStyle("_____ жылы _________________________________________________ оқу орнын бітірген."));
        document.add(getUnderParagraph("(оқу орнының аты, қаласы)"));
    }

    private void createSecondBlockKaz(Document document) {
        document.add(new Paragraph("АРЫЗ")
                .setBold()
                .setTextAlignment(TextAlignment.CENTER));

        document.add(getParagraphBasicStyle("Мені Astana IT University-дің Бакалавриат бағдарламасына жататын "));
        addParagraphWithUnderscoresAtTheStart(document, " оқыту бағдарламалар тобындағы");
        addParagraphWithUnderscoresAtTheStart(document, " білім бағдарламасы бойынша оқуға");
        document.add(getParagraphBasicStyle("қабылдауыңызды сұраймын."));
        document.add(getParagraphBasicStyle("№ ___________ ҰБТ сертификатының иесі ретінде _________ балл жинадым."));
    }

    private void createThirdBlockKaz(Document document) {
        document.add(getParagraphBasicStyle("Ата-ана немесе асыраушы ақпараты (Бакалавриат үшін): ")
                .setBold());
        addParagraphWithUnderscoresAtTheEnd(document, "Анасының/асыраушының Т.А.Ә: ");
        addParagraphWithUnderscoresAtTheEnd(document, "Жұмыс орны/қызметі: ");
        addParagraphWithUnderscoresAtTheEnd(document, "тел: _____________________________ e-mail: ");
        addParagraphWithUnderscoresAtTheEnd(document, "Әкесінің/асыраушының Т.А.Ә: ");
        addParagraphWithUnderscoresAtTheEnd(document, "Жұмыс орны/қызметі: ");
        addParagraphWithUnderscoresAtTheEnd(document, "тел: _____________________________ e-mail: ");
        document.add(getParagraphBasicStyle("Отбасындағы бала саны: ______"));
    }

    private void createFourthBlockKaz(Document document) {
        document.add(getParagraphBasicStyle("Қосымша мәлімет")
                .setBold()
                .setTextAlignment(TextAlignment.CENTER));

        document.add(
                new Paragraph(new Text("1. Мүгедектік ")).addStyle(basicStyle)
                        .add(new Text("(тиісті сөздің астын сызу керек) ").setItalic().setFontSize(10))
                        .add("бар / жоқ")
        );
        document.add(getParagraphBasicStyle("Топ № __________, категория (бала кезден / кейін пайда болған) ________________"));
        document.add(getParagraphBasicStyle("Куәлік № ______________, берілген күні  ______________________"));
        document.add(
                new Paragraph(new Text("2. Жетім ")).addStyle(basicStyle)
                        .add(new Text("(тиісті сөздің астын сызу керек) ").setItalic().setFontSize(10))
                        .add("тұл/ шешесіз, әкесіз")
        );
        document.add(getParagraphBasicStyle("3.Ерекше белгілер (\"Алтын белгі\", оқушылар арасындағы республикалық және халықаралық ғылыми жарыстардың, жалпы білім пәндері олимпиадаларының, орындаушылар конкурсының, спорт жарыстарының жеңімпазы немесе жүлдегері)"));
        addParagraphWithUnderscoresAtTheEnd(document,"\n");
        addParagraphWithUnderscoresAtTheEnd(document,"\n");
        addParagraphWithUnderscoresAtTheEnd(document,"4. Оқу кезінде қайда тұратын боласыз? – өз үйімде, туыстарымда, пәтерде өзім / достарыммен, жатақханада (қайсысы екенін жазыңыз) ");
    }

    private void createFooterKaz(Document document){
        document.add(getParagraphBasicStyle(" «____»______________ 20____ ж.\t\t\t\t\t_____________ Абитуриенттің қолы"));
    }

    private Paragraph getUnderParagraph(String str) {
        return getParagraphWithUnderStyle(str).setTextAlignment(TextAlignment.CENTER);
    }

    private Paragraph getParagraphBasicStyle(String str) {
        return new Paragraph(str).addStyle(basicStyle);
    }

    private Paragraph getParagraphWithUnderStyle(String str) {
        return new Paragraph(str).addStyle(underParagraphStyle);
    }

    private void addParagraphWithUnderscoresAtTheEnd(Document document, String str) {
        float pageRightXPos = document.getPdfDocument().getDefaultPageSize().getRight() -
                document.getLeftMargin() - document.getRightMargin();
        Paragraph p = getParagraphBasicStyle(str)
                .add(new Tab()).addTabStops(new TabStop(pageRightXPos, TabAlignment.RIGHT, new SolidLine(0.5f)));
        document.add(p);
    }

    private void addParagraphWithUnderscoresAtTheStart(Document document, String str) {
        float pageRightXPos = document.getPdfDocument().getDefaultPageSize().getRight() -
                document.getLeftMargin() - document.getRightMargin();
        Paragraph p = new Paragraph()
                .add(new Tab()).addTabStops(new TabStop(pageRightXPos, TabAlignment.RIGHT, new SolidLine(0.5f)))
                .add(getParagraphBasicStyle(str));
        document.add(p);
    }
}
